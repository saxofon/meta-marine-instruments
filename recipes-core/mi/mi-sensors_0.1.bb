SUMMARY = "Marine Instrument Server - export/import sensor data, boat data etc."
DESCRIPTION = "This is the server part of the marine instrument project. Also includes a simple simulator to ease instrument development."
HOMEPAGE = "https://gitlab.com/saxofon/marine-instruments"
SECTION = "console/tools"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1ebbd3e34237af26da5dc08a4e440464"

DEPENDS += "libwebsockets"
DEPENDS += "libiio"
DEPENDS += "protobuf protobuf-native"
DEPENDS += "protobuf-c protobuf-c-native"

SRC_URI = "git://gitlab.com/saxofon/marine-instruments.git;protocol=https;branch=topic/sensors"
SRCREV = "d3c664c7f1b841f65af9dffc103f620e2addc374"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SRC_URI += "file://mi-sensors"

S = "${WORKDIR}/git"

TARGET_CC_ARCH += "${LDFLAGS}"

do_compile() {
	oe_runmake -C ${S}/src/protobufs
	oe_runmake -C ${S}/src/mi-sensors
}

inherit update-rc.d
INITSCRIPT_PACKAGES = "${PN}"
INITSCRIPT_NAME = "mi-sensors"
INITSCRIPT_PARAMS = "defaults 99"
FILES:${PN} += "${INIT_D_DIR}/mi-sensors"

do_install() {
	oe_runmake -C ${S}/src/mi-sensors 'DESTDIR=${D}' install
	install -d ${D}${INIT_D_DIR}
	install -m 0755 ${WORKDIR}/mi-sensors ${D}${INIT_D_DIR}/mi-sensors
}
