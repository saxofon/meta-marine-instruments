SUMMARY = "Marine Instrument Dashboard - export/import sensor data, boat data etc."
DESCRIPTION = "This is the dashboard part of the marine instrument project."
HOMEPAGE = "https://gitlab.com/saxofon/marine-instruments"
SECTION = "console/tools"
LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1ebbd3e34237af26da5dc08a4e440464"

DEPENDS += "protobuf protobuf-native"
DEPENDS += "protobuf-c protobuf-c-native"
RDEPENDS:${PN} += "lighttpd"
RDEPENDS:${PN} += "closure"
RDEPENDS:${PN} += "protobuf-js"
RDEPENDS:${PN} += "two.js"

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI = "git://gitlab.com/saxofon/marine-instruments.git;protocol=https"
SRCREV = "d3c664c7f1b841f65af9dffc103f620e2addc374"

S = "${WORKDIR}/git"

TARGET_CC_ARCH += "${LDFLAGS}"

FILES:${PN} += "/usr/share/mi-dashboard"

do_compile() {
	oe_runmake -C ${S}/src/protobufs
}

do_install() {
	oe_runmake -C ${S}/src/mi-dashboard 'DESTDIR=${D}' install
	ln -s /usr/lib/js/closure ${D}/usr/share/mi-dashboard
	ln -s /usr/lib/js/protobuf-js ${D}/usr/share/mi-dashboard
	ln -s /usr/lib/js/two ${D}/usr/share/mi-dashboard
}
