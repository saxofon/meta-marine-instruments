FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI += "file://lighttpd.conf"

RDEPENDS:${PN} += "lighttpd-module-proxy"
